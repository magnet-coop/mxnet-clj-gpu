#!/usr/bin/env bash

set -eu

DIFF="format.diff"

docker run \
       --rm \
       --volume "$(pwd)":/code \
       --workdir /code \
       --entrypoint /bin/sh \
       jamesmstone/shfmt \
       -c "find . -name '*.sh' | xargs shfmt -d -i 4" |
       tee "${DIFF}"

if [[ "$(wc -c ${DIFF} | cut -c1)" -gt 0 ]]; then
    exit 1
fi
