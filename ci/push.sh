#!/usr/bin/env bash

set -eu

TAG="${BITBUCKET_COMMIT:=local}"
TAG=$(echo "${TAG}" | cut -c-7)

# Login to Dockerhub
# shellcheck disable=SC2091
./ci/dockerhub-login.sh

docker tag magnetcoop/mxnet-clj-gpu:latest "magnetcoop/mxnet-clj-gpu:${TAG}"
docker tag magnetcoop/mxnet-clj-gpu:latest "magnetcoop/mxnet-clj-gpu:latest"

docker push "magnetcoop/mxnet-clj-gpu:${TAG}"
docker push "magnetcoop/mxnet-clj-gpu:latest"
